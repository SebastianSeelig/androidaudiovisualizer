package de.codeyourapp.audiovisualizer
import android.Manifest
import android.content.pm.PackageManager
import android.media.audiofx.Equalizer
import android.media.audiofx.Visualizer
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.hypot
import kotlin.math.log10


class MainActivity : AppCompatActivity() {

    companion object {
        lateinit  var mRawAudioBytes: ByteArray
        private val targetEndpoints = listOf(0f, 63f, 160f, 400f, 1000f, 2500f, 6250f, 16000f)
        private val DESIRED_CAPTURE_SIZE = 1024
        private lateinit var frequencyOrdinalRanges: List<IntRange>

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), 10)

        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.MODIFY_AUDIO_SETTINGS
            ) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.MODIFY_AUDIO_SETTINGS),
                10
            )

        val equalizer = Equalizer(0, 0)
        equalizer.enabled = true
        val visualizer = Visualizer(0)
        visualizer.scalingMode = 0
        val captureSizeRange = Visualizer.getCaptureSizeRange().let { it[0]..it[1] }
        val captureSize = DESIRED_CAPTURE_SIZE.coerceIn(captureSizeRange)
        visualizer.captureSize = captureSize
        val samplingRate = visualizer.samplingRate/1000
        frequencyOrdinalRanges = targetEndpoints.zipWithNext { a, b ->
            val startOrdinal = 1+ (captureSize * a / samplingRate).toInt()
            val endOrdinal = (captureSize * b / samplingRate).toInt()
            startOrdinal..endOrdinal
        }




        visualizer.setDataCaptureListener(object : Visualizer.OnDataCaptureListener {
            override fun onWaveFormDataCapture(
                vis: Visualizer,
                bytes: ByteArray,
                samplingRate: Int
            ) {
            }

            override fun onFftDataCapture(
                visualizer: Visualizer,
                fft: ByteArray,
                samplingRate: Int
            ) {
                val output = FloatArray(frequencyOrdinalRanges.size)

                for ((i, frequencyOrdinalRange) in frequencyOrdinalRanges.withIndex()) {
                    var logMagnitudeSum = 0f
                    for (k in frequencyOrdinalRange) {
                        val fftIndex = k * 2
                        logMagnitudeSum += log10(hypot(fft[fftIndex].toFloat(), fft[fftIndex + 1].toFloat()))
                    }
                    if (logMagnitudeSum.isInfinite())
                        logMagnitudeSum = 0F
                    output[i] = logMagnitudeSum / (frequencyOrdinalRange.last - frequencyOrdinalRange.first + 1)
                }
                zero_red.text = (output[0]/(log10(hypot(127f, 127f)))).toString()
                one_orange.text = (output[1]/(log10(hypot(127f, 127f)))).toString()
                two_yellow.text = (output[2]/(log10(hypot(127f, 127f)))).toString()
                three_green.text = (output[3]/(log10(hypot(127f, 127f)))).toString()
                four_blue.text = (output[4]/(log10(hypot(127f, 127f)))).toString()
                five_indigo.text = (output[5]/(log10(hypot(127f, 127f)))).toString()
                six_violet.text = (output[6]/(log10(hypot(127f, 127f)))).toString()
                // If you want magnitude to be on a 0..1 scale, you can divide it by log10(hypot(127f, 127f))
                // Do something with output
            }

        }, Visualizer.getMaxCaptureRate(), false, true)
        visualizer.enabled = true

    }





       /* val visualizer = Visualizer(0)
        visualizer.enabled = true
        val visText = visualizer.captureSize.toString()
       // val visText = visualizer.getWaveForm(Visualizer.getMaxCaptureRate()).toString()
        textView.text = visText*/





        override fun onDestroy() {
            super.onDestroy()
            //visualizer.release()
        }

}
